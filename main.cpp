#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <math.h>
/*//////////////////////////////////////
                Zestaw 1
//////////////////////////////////////*/

// ZADANIE 1
/**
 *Napisz definicję klasy pochodnej do podanej, która będzie wymuszała by wszystkie liczby
 * w wektorze były tego samego znaku
 */
class Cmoja_1_1 {
private:
    double Fvect[3];
public:
    Cmoja_1_1() {}

    virtual void change(double _v, int _i) {
        if (_i >= 0 && _i < 3) {
            Fvect[_i] = _v;
        }
    }

    virtual void negate() {
        for (int i = 0; i < 3; i++) {
            Fvect[i] = -Fvect[i];
        }
    }

    virtual double get(int _i) {
        if (_i >= 0 && _i < 3) {
            return Fvect[_i];
        }
        return 0;
    }
};

class Cmoja_1_1_rozwiazanie : private Cmoja_1_1 {
private:
    bool is_negative = false;
public:
    Cmoja_1_1_rozwiazanie() {
        for (int i = 0; i > 3; i++) {
            Cmoja_1_1::change(0, i);
        }
    }

    virtual void change(double _v, int _i) override {
        if (_i < 0 || _i >= 3) {
            return;
        }
        if ((_v > 0 && is_negative) || (_v < 0 && !is_negative)) {
            Cmoja_1_1::change(-_v, _i);
        } else {
            Cmoja_1_1::change(_v, _i);
        }
    }

    void set_is_negative(bool is_negative) {
        if (this->is_negative ^ is_negative) {
            Cmoja_1_1::negate();
            this->is_negative = is_negative;
        }
    }

    virtual double get(int _i) override {
        return Cmoja_1_1::get(_i);
    }
};
// ZADANIE 2
/**
 *przerób klasę z zadania 1 na szablon obsługujący N elementów typu T (n to pozatypowy parametr szablonu)
 */
template<unsigned int N, typename T>
class Cmoja_1_2 {
private:
    T Fvect[N];
public:
    Cmoja_1_2() {}

    virtual void change(T _v, int _i) {
        if (_i >= 0 && _i < N) {
            Fvect[_i] = _v;
        }
    }

    virtual void negate() {
        for (int i = 0; i < N; i++) {
            Fvect[i] = -Fvect[i];
        }
    }

    virtual T get(int _i) {
        if (_i >= 0 && _i < N) {
            return Fvect[_i];
        }
        return T();
    }
};

// ZADANIE 3
/*
 * ile bajtów minimalnie potrzeba na przechowanie jednego elementu typu B
 * zakładając że sizeof(int) = 4
 */
class A_1_3 {
private:
    int Pole;
public:
    A_1_3() { Pole = 10; }
};

class B_1_3 : public A_1_3 {
private:
    int Objetosc, Pole;
public:
    B_1_3() : A_1_3() { Pole = 10; }
};
//ODPOWIEDŹ  =12

// ZADANIE 4
/**
 *Uzupełnij podają klasę o kod operator by kod poniżej skompilował się prawidłowo
 * i wydrukował wartości od 1 do 10
 *
 * int main(){
 *  Generator a(1);
 *     for(auto i=0; i<10;i++){
 *       std::cout << a() << std::endl;
 *     }
 * }
 */
class Generator {
private:
    int aktualny;
public :
    Generator(int _a = 0) : aktualny(_a) {}

    int operator()() {
        return aktualny++;
    }
};

// ZADANIE 5
/**
 * Napisz funkcję drukującą od końca wszystkie pary klucz - wartość z kontenera zdefiniowanego
 * std::map<std::string, int>
 */
void zadanie_1_5() {
    std::map<std::string, int> map;
    map.emplace("one", 1);
    map.emplace("two", 2);
    map.emplace("three", 3);
    auto riter = map.rbegin();
    for (; riter != map.rend(); riter++) {
        std::cout << "key: " << riter->first << " val: " << riter->second << std::endl;
    }
}


/*//////////////////////////////////////
                Zestaw 2
//////////////////////////////////////*/

// ZADANIE 1

/*
Napisz definicje klasy pochodnej do podanej która będzie wymuszała aby wartości
w wektorze napisów zawsze zaczynały się od litery 'a' (nawet po pierwszym utworzeniu obiektu)
*/
class Cmoja_2_1 {
private:
    std::string Fvect[3];
public:
    Cmoja_2_1() {}

    virtual void change(std::string _v, int _i) {
        if (_i >= 0 && _i < 3) {
            Fvect[_i] = _v;
        }
    }

    virtual std::string get(int _i) {
        if (_i >= 0 && _i < 3) {
            return Fvect[_i];
        }
        return "a";
    }
};

class Rozwiazanie_2_1 : public Cmoja_2_1 {
public:
    Rozwiazanie_2_1() {
        for(int i =0; i< 3; i++){
            Cmoja_2_1::change("a", i);
        }
    }

    void change(std::string _v, int _i) override {
        if(_v.front() == 'a') {
            Cmoja_2_1::change(_v, _i);
        } else {
            Cmoja_2_1::change(std::string(1, 'a').append(_v), _i);
        }
    }
};

// ZADANIE 2
/**
 * Napisz szablon klasy bufora przechowującego n elementów typu T. Szablon ma udostępniać
 * operator[] lub odpowiadającą mu funkcję  i nie musi być zabezpieczony przed przekroczeniem
 * pamięci
 */

template<unsigned int N, typename T>
class bufor_2_2 {
private:
    T buf[N];
    int position = 0;
public:
    void insert(T elem) {
        if (position == N) {
            position = 0;
        }
        buf[position++] = elem;
    }

    const T &operator[](int index) {
        return buf[index];
    }
};

// ZADANIE 3
/**
 * Zmodyfikuj podany kod usuwając z niego błąd który powoduje wyciek pamięci
 */
class Cmoja_2_3 {
private:
    double *Fdane;
public:
    Cmoja_2_3() {
        Fdane = new double[100];
    }

    Cmoja_2_3(const Cmoja_2_3 &r) {
        Fdane = r.Fdane;
    }

    // ~Cmoja_2_3() { delete[] Fdane; }
    // ROZWIĄZANIE: klasa ma metody wirtualne, ale nie wirtualny konstruktor
    // https://stackoverflow.com/questions/10024796/c-virtual-functions-but-no-virtual-destructors
    virtual ~Cmoja_2_3() { delete[] Fdane; }

    virtual void change(double v, int i) {
        if (i >= 0 && i < 100) {
            Fdane[i] = v;
        }
    }

    virtual double get(int i) {
        if (i >= 0 && i < 100) {
            return Fdane[i];
        }
        return 0;
    }
};

//ZADANIE 4

/**
 * Napisz kod kopiujący ze zmiennej std::vector<std::string> A do zmiennej std::set<std::string> B
 * unikalne napisy
 */
void rozwiazanie_2_4() {
    std::vector<std::string> A;
    A.push_back("ASD");
    A.push_back("ASD");
    A.push_back("DUPA");

    std::set<std::string> B;
    B.insert(A.begin(), A.end());
}


/*//////////////////////////////////////
                Zestaw 3
//////////////////////////////////////*/

//ZADANIE 1
/**
 * Napisz definicję klasy pochodnej do
 * podanej w której będziemy mieli pewność iż pole masa ma zawsze wartość większą od 10
 */

class CCos_3_1 {
private:
    double masa;
protected:
    virtual void setMasa(double _i) {
        masa = _i;
    }

public:
    CCos_3_1() {}
};

class Rozwiazanie_3_1 : private CCos_3_1 {
private:
    const std::string INVALID_VALUE = "za mała masa";

    void validate_value(double masa) {
        if (masa <= 10) {
            throw std::invalid_argument(INVALID_VALUE);
        }
    }

public:
    Rozwiazanie_3_1(double masa) : CCos_3_1() {
        setMasa(masa);
    }

    virtual void setMasa(double _i) override {
        validate_value(_i);
        CCos_3_1::setMasa(_i);
    }
};


//ZADANIE 2
/**
 * Przygotuj szablon funkcji sumującej co x-ty element z dowolnego kontenera STL
 * (x na być parametrem metody a nie pozatypowym parametrem szablonu. Przykładowo,
 * dla x = 3 sumowany ma być co trzeci element)
 */
template<typename T>
typename T::value_type rozwiazanie_3_2(T container, unsigned int x) {
    typedef typename T::value_type return_type;
    auto sum = return_type();
    unsigned int counter = 0;
    for (auto const &elem: container) {
        if (++counter == x) {
            sum += elem;
            counter = 0;
        }
    }
    return sum;
}

//ZADANIE 3
/**
 *  Jakie wartości są we wszystkich polach obiektu b po wykonaniu poniższego kodu:
 */

class A_3_3 {
private:
    std::string nazwa;
public:
    A_3_3() { nazwa = "M"; }
};

class B_3_3 : public A_3_3 {
private:
    int objetosc;
    int nazwa;
public:
    B_3_3() : A_3_3(), nazwa(5) {
        objetosc = nazwa * nazwa;
        nazwa = 10;
    }
};

int main_3_3() {
    B_3_3 b;
    // A => nazwa = "M";
    // B => objetosc = 25;
    // B => nazwa = 10;
}

//ZADANIE 4
/**
 * Uzupełnik podaną klasę o kod operator by kod poniżej
 * skopiował się prawidłowo i wydrukował wartości funkcji sinus o amplitudzie 5 z zakresu
 * 0-2*pi z krokiem 0.2
int main(){
    Generator_3_4 gen(5);
    for(auto i=0.0; i < 2* M_PI; i+=0.2){
        std::cout << gen(i);
    }
}

*/
class Generator_3_4 {
private:
    double Amp;
public:
    Generator_3_4(double _a = 1) : Amp(_a) {}

    double operator()(double x) {
        return Amp * std::sin(x);
    }
};

//ZADANIE 5
/**
 * Napisz funkcję zmieniającą w kontenerze std::set<std::string> wszustkie
 * napisy dłuższe niż zadana wartość x (parametr funkcji)  na napis okrojony do x
 * liter i uzupełniony 3 kropkami (przykładowo dla x =3 i napisu "alabaster"
 * powinien zostać zmieniony na "ala...")
 */
void rozwiazanie_3_5(std::set<std::string> &set, unsigned int x) {
    for (auto const &elem : set) {
        if (elem.size() > x) {
            auto temp = elem.substr(0, x);
            temp += "...";
            set.erase(elem);
            set.emplace(temp);
        }
    }
}


/*//////////////////////////////////////
                Zestaw 4
//////////////////////////////////////*/

//ZADANIE 1
/**
 * Napisz definisję klasy pochodnej do podanej w której będziemy mieli pewność
 * iż każdy element wektora zawiera znak z przedziału <'a', 'z'>
 */
class CCos_4_1 {
private:
    char FVect[3];
protected:
    virtual void setValue(int idx, char val) {
        FVect[idx] = val;
    }

public:
    CCos_4_1() {}
};

class rozwiazanie_4_1 : private CCos_4_1 {
public:
    rozwiazanie_4_1() {
        for (int i = 0; i < 3; i++) {
            CCos_4_1::setValue(i, 'a');
        }
    }

    virtual void setValue(int idx, char val) override {
        if (val < 'a' || val > 'z' || idx > 2 || idx < 0) {
            return;
        }
        CCos_4_1::setValue(idx, val);
    }
};

//ZADANIE 2
/**
 * Przygotuj szablon klasy umożliwiającej przechowywanie n elementów typu T
 * zapewniającej jednocześnie że wszystkie elementy mieszczą się w zakresie
 * od a do b (próba wstawienia nieprawidłowych wartości ma byc ignorowana)
 * N jest pozatypowym parametrem szablonyu
 */
template<unsigned int N, typename T, T lower_bound, T upper_bound>
class rozwiazanie_4_2 {
private:
    T buf[N];
    unsigned int position = 0;
public:
    void push_back(T elem) {
        if (elem > upper_bound || elem < lower_bound) {
            return;
        }
        if (position == N) {
            position = 0;
        }
        buf[position++] = elem;
    }
};

//ZADANIE 3
/**
 * W podanym kodzie naszym celem jest dziedziczenie wielokrotne z A w klasie C
 * przy czym chcemy by w klasie było tylko jedno pole X typu int.
 * W kodzie są błędy które sprawiają ze nie da się go skompilować.
 * Wyjaśnij na czym polega błąd i wprowadź poprawkę.
 */
//ZADANIE CHYBA NIE MA SENSU. KOD PODOBNY DO 3_3
//NIE PASUJE DO TREŚCI
class A_4_3 {
private:
    std::string nazwa;
    char znak;
public:
    //BŁĄD
    //TAK BYŁO WCZEŚNIEJ:
    //A_4_3(), znak('a') {
    A_4_3() : znak('a') {
        nazwa = "M";
    }
};

class B_4_3 : public A_4_3 {
private:
    int objetosc;
    int nazwa;
public:
    B_4_3() : A_4_3(), nazwa(5) {
        objetosc = nazwa + nazwa;
        nazwa = 12;
    }
};

int main_4_3() {
    B_4_3 b;
}

//ZADANIE 4
/**
 * Przekształć klasę z zadania 1 dodając do niej kontrolę poprawności indeksu
 * idx oraz rzucania wyjątków w przypadku przekroczenia tego indeksu.
 * Podaj przykład przechwycenia wyjątku.
 */
class rozwiazanie_4_4 {
private:
    char FVect[3];
//LEKKA ZMIANA NA PUBLIC BO CHYBA NIE BAZOWAŁBY NA ROZWIĄZANIU ZADANIA 4_1
public:
    virtual void setValue(int idx, char val) {
        if (idx < 0 || idx >= 3) {
            throw std::invalid_argument("index wychodzi poza dozwolony zakres");
        }
        FVect[idx] = val;
    }

public:
    rozwiazanie_4_4() {}
};

int main_4_4() {
    rozwiazanie_4_4 roz;
    try {
        roz.setValue(123, 'a');
    } catch (const std::invalid_argument &ex) {
        std::cerr << ex.what();
    }
}

//ZADANIE 5
/**
 * Napisz funkcję która w kontenerze std::set<std::string> wszystkie napisy dłuższe
 * niż zadana warość (parametr funkcji) zamieni na napis ograniczony do x-4 znaków, usupełniony
 * 3 kropkami oraz ostatnim znakiem orginalnego napisu. Przykładowo:
 * x=7 napis "alabaster" powinien zostać zamieniony na "ala...r"
 */
void rozwiazanie_4_5(std::set<std::string> &set, unsigned int limit) {
    for (auto const &elem : set) {
        auto size = elem.size();
        if (size > limit) {
            auto temp = elem.substr(0, limit - 4);
            temp += "..." + elem.substr(size - 1, size);
            set.erase(elem);
            set.emplace(temp);
        }
    }
}

int main() {
    return 0;
}
